import React, { Component } from 'react'
import PropTypes from 'prop-types'
import PanelButton from './panel_button'
import { setRecordSuspendStatus, getRecordSuspendStatus } from '../lib/store'
import * as styles from './styles'


export default class Panel extends Component {
  static propTypes = {
    className: PropTypes.string,
    style: PropTypes.object,
    eventEmitter: PropTypes.object.isRequired,
  }

  static defaultProps = {
    className: '',
    position: 'bottomRight',
  }

  render() {
    const { position, className, style, eventEmitter } = this.props

    const additionalPanelStyles = {}

    if (typeof position === 'string') {
      switch (position) {
        case 'topRight':
          additionalPanelStyles.top = '-2px'
          additionalPanelStyles.right = '20px'
          break
        case 'bottomRight':
          additionalPanelStyles.bottom = '-2px'
          additionalPanelStyles.right = '20px'
          break
        case 'bottomLeft':
          additionalPanelStyles.bottom = '-2px'
          additionalPanelStyles.left = '20px'
          break
        case 'topLeft':
          additionalPanelStyles.top = '-2px'
          additionalPanelStyles.left = '20px'
          break
      }
    } else {
      Object.assign(additionalPanelStyles, position)
    }

    return (
      <div>
        <div
          className={className}
          style={Object.assign({}, styles.panel, additionalPanelStyles, style)}
        >
          <PanelButton
            id={'buttonRecord'}
            active={getRecordSuspendStatus()}
            onToggle={active => {
              setRecordSuspendStatus(active)
              eventEmitter.emit('toggleRecord', { active })
              this.forceUpdate()
            }}
          />
        </div>
      </div>
    )
  }
}
