import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as styles from './styles'

export default class PanelButton extends Component {
  static props = {
    onToggle: PropTypes.bool.isRequired,
    active: PropTypes.bool.isRequired,
    name: PropTypes.oneOf(['buttonUpdates', 'buttonGraph', 'buttonLog'])
      .isRequired,
  }

  state = {
    hovered: false,
  }

  handleMouseOver = () => this.setState({ hovered: true })
  handleMouseOut = () => this.setState({ hovered: false })
  handleClick = () => {
    this.props.onToggle(!this.props.active)
  }

  render() {
    const { id, active } = this.props
    const { hovered } = this.state

    const additionalStyles = (() => {
      switch (id) {
        case 'buttonRecord':
          return active ? styles.buttonRecordOn : styles.buttonRecordOff
      }
    })()

    const title = (() => {
      switch (id) {
        case 'buttonRecord':
          return 'Toggle record suspend data'
      }
    })()

    const finalSyles = Object.assign(
      {},
      styles.button,
      additionalStyles,
      active && styles.button.active,
      hovered && styles.button.hover
    )

    return (
      <button
        type="button"
        onClick={this.handleClick}
        title={title}
        style={finalSyles}
        onMouseOver={this.handleMouseOver}
        onMouseOut={this.handleMouseOut}
      />
    )
  }
}
