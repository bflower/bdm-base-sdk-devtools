export const panel = {
    position: "fixed",
    height: "26px",
    backgroundColor: "#fff",
    color: "rgba(0, 0, 0, 0.8)",
    borderRadius: "2px",
    borderStyle: "solid",
    borderWidth: "1px",
    borderColor: "rgba(0, 0, 0, 0.1)",
    zIndex: "65000",
    fontFamily: "Helvetica, sans-serif",
    display: "flex",
    padding: "0 5px"
}

export const button = {
    opacity: 0.45,
    background: "transparent none center / 16px 16px no-repeat",
    width: "26px",
    margin: "0 5px",
    cursor: "pointer",
    border: "none",
    hover: {
        opacity: 0.7
    },
    active: {
        opacity: 1,
        ":hover": {
            opacity: 1
        }
    }
}

export const buttonRecordOn = {
    backgroundImage: `url(${require("./record_on.svg")})`
}

export const buttonRecordOff = {
    backgroundImage: `url(${require("./record_off.svg")})`
}

