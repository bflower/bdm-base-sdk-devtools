export function getRecordSuspendStatus() {
  const r = window.localStorage.getItem('bdm-base-sdk.record-suspend-status')
  return JSON.parse(r === null ? true: r)
}

export function setRecordSuspendStatus(v) {
  window.localStorage.setItem('bdm-base-sdk.record-suspend-status', JSON.stringify(v))
}
