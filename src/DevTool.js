import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { getRecordSuspendStatus } from './lib/store'
import Panel from './panel'
import EventEmmiter from 'events'
const eventEmitter = new EventEmmiter()


export default class DevTool extends Component {
  static bindInterpreter = function (interpreter, eventType = 'toggleRecordSuspendData') {
    eventEmitter.on('toggleRecord', ({ active }) => {
      interpreter.send({
        type: eventType,
        active,
      })
    })

  }
  static getRecordSuspendStatus = getRecordSuspendStatus

  static propTypes = {
    noPanel: PropTypes.bool,
    className: PropTypes.string,
    style: PropTypes.object,
    position: PropTypes.oneOfType([
      PropTypes.oneOf(['topRight', 'bottomRight', 'bottomLeft', 'topLeft']),
      PropTypes.shape({
        top: PropTypes.string,
        right: PropTypes.string,
        bottom: PropTypes.string,
        left: PropTypes.string,
      }),
    ]),
  }

  static defaultProps = {
    noPanel: false,
    className: '',
  }

  render() {
    const { noPanel, className, style } = this.props
    return (
      <div>
        {noPanel !== true && (
          <Panel
            position={this.props.position}
            className={className}
            eventEmitter={eventEmitter}
            style={style}
          />
        )}
      </div>
    )
  }
}
