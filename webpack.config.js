const { join } = require('path')
const CleanWebpackPlugin = require('clean-webpack-plugin')

module.exports = {
  mode: process.env.NODE_ENV,
  entry: {
    index: join(__dirname, 'src', 'index.js'),
    fakelms: join(__dirname, 'src', 'fakelms.js')
  },
  output: {
    libraryTarget: 'commonjs2',
    path: join(__dirname, 'cjs2'),
    filename: '[name].js',
  },
  resolve: {
    extensions: ['.js'],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: { cacheDirectory: true },
        },
      },
      {
        test: /\.svg$/,
        use: 'url-loader',
      },
    ],
  },
  externals: {
    react: {
      commonjs: 'react',
      commonjs2: 'react',
    },
  },
  plugins: [
    new CleanWebpackPlugin(['cjs2']),
  ],

}
