# bdm-base-sdk-devtools

DevTools for bdm-base-sdk to enable/disable suspend data recording.

## Installation

`npm install --save-dev bdm-babse-sdk-devtools`

## Usage

Somewhere in your application, create a DevTools component:

```js
import DevTools from 'bdm-base-sdk-devtools'

class MyApp extends React.Component {
  render() {
    return (
      <div>
        ...
        <DevTools />
      </div>
    );
  }
}
```

Supported props:

* `noPanel` — boolean, if set, do not render control panel
* `position` — object, position of control panel, default: `{ top: -2, right: 20 }` or
* `position` — string or object, `topRight`, `bottomRight`, `bottomLeft` or `topLeft`, default: `bottomRight`
* `className` — string, className of control panel
* `style` — object, inline style object of control panel



## Usage with FakeLMS (fake scorm api support)

````js
// in /src/devtools
// index.dev.js
import BdmSdkDevTools from '@b-flower/bdm-base-sdk-devtools'
import FakeLms from '@b-flower/bdm-base-sdk-devtools/cjs2/fakelms'

export {
  BdmSdkDevTools,
  FakeLms,
}
````

````js
// in /src/devtools
// index.prod.js
const BdmSdkDevTools = () => null

export {
  BdmSdkDevTools,
}
````

````js
// in /src/devtools
// index.js

/* global __DEV__ */
if (__DEV__) {
  const {
    BdmSdkDevTools,
    FakeLms
  } = require('./index.dev')

  FakeLms.attachLMSAPIToWindow('my-module-name')

  module.exports = { BdmSdkDevTools }
} else {
  const {
    BdmSdkDevTools,
  } = require('./index.prod')
  module.exports = { BdmSdkDevTools }
}
````